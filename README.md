# Dockerize a .NET Core application
by Rusl�n Gonz�lez

Source:[Docker Website](https://docs.docker.com/engine/examples/dotnetcore/) 

## Introduction

This example demonstrates how to dockerize an ASP.NET Core application.

Simple git clone this repo and use it

## 1. Prerequisites

This example assumes you already have an ASP.NET Core app on your machine. If you are new to ASP.NET you can follow a simple tutorial to initialize a project or clone our ASP.NET Docker Sample.

## 2. Modify the Dockerfile for an ASP.NET Core application

Modify the Dockerfile for your project folder.
Add the text below to your Dockerfile for either Linux or Windows Containers. The tags below are multi-arch meaning they pull either Windows or Linux containers depending on what mode is set in Docker for Windows. Read more on switching containers.
The Dockerfile assumes that your application is called aspnetapp. Change the Dockerfile to use the DLL file of your project.

```
FROM microsoft/dotnet:sdk AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY *.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . ./
RUN dotnet publish -c Release -o out

# Build runtime image
FROM microsoft/dotnet:aspnetcore-runtime
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "aspnetapp.dll"]
```

To make your build context as small as possible add a .dockerignore file to your project folder and copy the following into it.

```
bin\
obj\
```

## 4. Build and run the Docker image

Open a command prompt and navigate to your project folder.
Use the following commands to build and run your Docker image:

```
$ docker build -t aspnetapp .
$ docker run -d -p 8080:80 --name myapp aspnetapp
```

## That's it!

Go to localhost:8080 to access your app in a web browser.

